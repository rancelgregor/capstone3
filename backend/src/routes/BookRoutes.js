const express = require('express')

const BookRouter = express.Router();

const BookModel = require('../models/Book')

// Add Book
BookRouter.post('/addbook', async(req, res) => {
    try{
        let book = BookModel({
            title: req.body.name,
            author: req.body.author,
            genre: req.body.genre,
            coverImg: req.body.coverImg,
            availability: req.body.occupants,
            price: req.body.price,
            isAvailable: req.body.availability
        })

        const saveBook = await BookModel.save();
        res.send(saveBook);
    }catch(e){
        res.status(401).send('Bad Request, please try again');
    }
});


BookRouter.get('/books',async(req,res)=>{
    try{
        const books = await BookModel.find();
        res.send(books)
    }catch(e){
        res.status(401).send('Bad Request, Please try again')
    }
})

// Delete Book

BookRouter.delete('/deletebook',async(req,res)=>{
    try{
        const book = await BookModel.findByIdAndDelete(req.body.id);
        res.send(book)
    }catch(e){
        res.status(401).send('Bad Request, Please try again')
    }
})

// Update Book
BookRouter.patch('/updatebook',async(req,res)=>{
    try{
        let updates = {
            title: req.body.name,
            author: req.body.author,
            genre: req.body.genre,
            coverImg: req.body.coverImg,
            availability: req.body.occupants,
            price: req.body.price,
            isAvailable: req.body.availability
        }
        let book = await RoomModel.findByIdAndUpdate(req.body.id,updates,{new:true})
        res.send(book)
    }catch(e){
        res.status(401).send('Bad Request. Please try again')
    }
});

module.exports = BookRouter;