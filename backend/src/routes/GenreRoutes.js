const express = require('express');

const GenreRouter = express.Router();

const GenreModel = require('../models/Genre');

GenreRouter.post('/addGenre', async(req, res) =>{
    try{
        let genre = GenreModel({
            name: req.body.name,
            description: req.body.description,
            imgPath: req.body.imgPath
        });

        let genreToSave = await genre.save();
        res.send(genreToSave);     
    }catch(e){
        res.status(401).send('Bad request. please try again')
    }
});

GenreRouter.get('/genres', async(req, res) => {
    try{
        genres = await GenreModel.find();
        res.send(genres);
    }catch(e){
        res.status(401).send('Bad Request, please try again')
    }
});


GenreRouter.delete('/deleteGenre', async(req, res) => {
    try{
        let deleteBook = await GenreModel.findOneAndDelete(req.body.id)
        res.send(deleteBook);
    }catch(e){
        res.status(401).send('Bad Request, please try again')
    }
});


GenreRouter.patch('/updateGenre', async(req, res) =>{

        let updates = GenreModel({
            name: req.body.name,
            description: req.body.description,
            imgPath: req.body.imgPath
        });
        try{
            let updateGenre = await GenreModel.findOneAndUpdate(req.body.id, updates, {new:true})
    }catch(e){
        res.status(401).send('Bad Reqquest, please try again')
    }
});

module.exports = GenreRouter;

