const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BookSchema = new Schema({
    title: String,
    author: [{
        type: String
    }] ,
    genre: {
        type: Schema.Types.ObjectId,
        ref: 'Genre'
    },
    location: {
        type: Schema.Types.ObjectId,
        ref: 'Location'
    },
    coverImg: String,
    Availability: Number,
    price: Number,
    isAvailable: {
        type: Boolean,
        default: true
    }
},
    { timestamps: true }
);

module.exports = mongoose.model('Book', BookSchema);