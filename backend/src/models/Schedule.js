const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ScheduleSchema = new Schema ({
    book: {
        type: Schema.Types.ObjectId,
        ref: 'Book'
    },
    genre: {
        type: Schema.Types.ObjectId,
        ref: 'Genre'
    },
    location: {
        type: Schema.Types.ObjectId,
        ref: 'Location'
    },
    startTime: String,
    endTime: String,
    date: [{
        type: String
    }],
    price: Number,
    isAvailable: {
        type: Boolean,
        default: true
    }
},
    { timestamps: true }
);

module.exports = mongoose.model('Schedule', ScheduleSchema);