const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const GenreModel = new Schema({
    name: String,
    description: String,
    imgPath: String
},
    {timestamps: true}
);

module.exports = mongoose.model('Genre', GenreModel);
