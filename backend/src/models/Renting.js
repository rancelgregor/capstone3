const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RentingSchema = new Schema ({
    schedule: {
        type: Schema.Types.ObjectId,
        ref: 'Schedule'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    status: {
        type: String,
        default: 'Pending'
    },
    payment: {
        type: Schema.Types.ObjectId,
        ref: 'Payment'
    },
    amount: Number
},
    { timestamps: true }
);


module.exports = mongoose.model('Renting', RentingSchema);