// call express
const express = require('express');

// create an instance of express
const app = express();

// call cors
const cors = require('cors');

// call mongoose
const mongoose = require('mongoose');

// call config file
const config = require('./src/config');

// call multer
const multer = require('multer');

// call path
const path = require('path'); 

// allow saving files in the public directory
app.use(express.static(path.join(__dirname, 'public')));

// allow parsing of data from url
app.use(express.urlencoded({extended:false}));

// allow parsing of data from a form
app.use(express.json());

// allow cors
app.use(cors());

// connect to database
mongoose.connect(config.databaseURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then( () => {
    console.log("Remote Database Connection Successful");
});

app.listen( config.PORT, () => {
    console.log(`Listening on Port ${config.PORT}`);
});

// location API
const bookRoutes = require('./src/routes/BookRoutes');
app.use('/admin', bookRoutes);

// location API
const genreRoutes = require('./src/routes/GenreRoutes');
app.use('/admin', genreRoutes);

// location API
const locationRoutes = require('./src/routes/LocationRoutes');
app.use('/admin', locationRoutes);

// location API
const scheduleRoutes = require('./src/routes/ScheduleRoutes');
app.use('/admin', scheduleRoutes);

// location API
const userRoutes = require('./src/routes/UserRoutes');
app.use('/admin', userRoutes);