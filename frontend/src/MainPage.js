import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Nav from './components/Nav';
import Loader from './components/Loader';


const MainPage = () => {
    const Landing = React.lazy(() => import('./views/Landing'));
    const Genres = React.lazy(() => import('./views/genres/Genres'));
    return (
        <React.Fragment>
            <Nav/>

        <BrowserRouter>
            <React.Suspense
            fallback = { Loader }
            >
                <Switch>
                    <Route 
                    path = {'/'}
                    exact
                    render = { props => <Landing { ...props }/>} />
                    <Route 
                    path = {'/genres'}
                    render = { props => <Genres { ...props }/>} />
                </Switch>
            </React.Suspense>
        
        
        
        </BrowserRouter>
            
        </React.Fragment>
      );
    }

export default MainPage;
