import React,{useState, useEffect} from 'react';
import { Container, Col, Row, Card, CardHeader, CardBody, Button } from 'reactstrap';
import GenreForm from './GenreForm';
import GenreCruds from './GenreCruds';

const Genres = props => {

    const [showForm, setShowForm] = useState(false);
    const [genres, setGenres] = useState([]);

    //fetch genres
    useEffect(()=>{
        fetch('http://localhost:4000/admin/genres')
        .then(res => res.json())
        .then(res => {
            console.log(res);
            setGenres(res);
        })
    },[]);




    //saving Genre
 

    const saveGenre = (name,description,imgPath) =>{


    
        fetch('http://localhost:4000/admin/addGenre', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                imgPath: imgPath,
                
            })
        }, []).then(res=>res.json())
        .then(res=>{
            setGenres([...genres, res]);
        })

        console.log(genres);
        setShowForm(false);
    }


    const toggleForm = () =>{
        setShowForm(!showForm);
    }




    return(
       <div>
           <Container>
               <Card className ="mt-5">
                   <CardHeader>
                       <Row>
                           <Col className="md-12 h1 text-center">Book Genres</Col>
                       </Row>
                       <Button
                       onClick = {()=> setShowForm(true)}>
                           + Add New Genre
                       </Button>
                   </CardHeader>
                   <CardBody>
                       <GenreCruds 
                       genres = {genres}/>
                   </CardBody>
               </Card>
           </Container>
            <GenreForm 
            modal = {showForm}
            toggleModal = {toggleForm}
            saveGenre = {saveGenre}/>
       </div>
    )
}

export default Genres;