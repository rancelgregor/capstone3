import React,{useState} from 'react'
import {Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input, Button} from 'reactstrap';

const GenreForm = props =>{


    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [imgPath, setImgPath] = useState("");

    const InitializeGenre = (name, description) =>{
        props.saveGenre(name, description);
        setName("");
        setDescription("");

    }
    return(
        <Modal
        isOpen = {props.modal}
        toggle = {props.toggleModal}>
            <ModalHeader className="col-md-12"
            toggle = {props.toggleModal} >
                    Add a new genre
            </ModalHeader>
            <ModalBody>
                <Form>
                    <FormGroup>
                        <Label className="text-center">Genre Name</Label>
                        <Input name="name" type="text"
                        onChange = { (e) => setName(e.target.value)}/>
                    </FormGroup>
                    <FormGroup>
                        <Label className="text-center">Desciption</Label>
                        <Input name="description" type="text"
                        onChange = {(e) => setDescription(e.target.value)}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Image File</Label>
                        <Input type="file" name="imgPath" onChange={(e)=> setImgPath(e.target.files[0])}></Input>
                    </FormGroup>
                    <Button className="col-md-12"
                    onClick = {()=> InitializeGenre(name,description,imgPath)}>Submit</Button>
                </Form>
            </ModalBody>
        </Modal>
    )
}

export default GenreForm;