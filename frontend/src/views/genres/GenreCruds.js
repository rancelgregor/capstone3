import React from 'react';
import {Card, CardHeader, CardBody, CardImg, Row, Col, CardFooter, Button} from 'reactstrap';

const GenreCruds = props => {
    return(
        <div>
            <Row>
                {props.genres.map((genre) =>(
                <Col sm>
                    <Card
                        key = {genre._id}>
                        <CardHeader>
                            {genre.name}
                        </CardHeader>
                        <CardImg>
                            {genre.imgPath}
                        </CardImg>
                        <CardBody>
                            {genre.description}
                        </CardBody>
                        <CardFooter>
                            <Button>Edit</Button>
                            <Button>Delete</Button>
                        </CardFooter>
                    </Card>
                </Col>
            ))}
            </Row>
        </div>
    )
}

export default GenreCruds;