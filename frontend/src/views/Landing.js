import React from 'react';
import Background from '../library.jpg';
import {Input, InputGroup, InputGroupAddon, Button} from 'reactstrap';
import { FaSearch } from 'react-icons/fa';
import styled from 'styled-components';

const StyledDiv = styled.div`
Input{
  font-size: 30px;
}
`;

const Landing = props => {
    return(
        <React.Fragment>
            <StyledDiv style = {{height: '90vh', backgroundRepeat: 'no-repeat', backgroundSize: '100% 100%', backgroundImage: `url(${Background})`}} 
            className ="border border-dark d-flex align-items-center justify-content-center">
                <div style = {{width: '35%', height: '10%', border: '1px solid black', borderRadius: '20px'}} className="d-flex rounded">
                <InputGroup>
                    <Input style={{height: '100%', width: '90%'}} placeholder="Type a book title or author name" />
                    <InputGroupAddon addonType="append">
                        <InputGroup style={{width: '75px'}}>
                            <Button style={{height: '100%', width: '100%'}}>
                                <FaSearch/>
                            </Button>
                        </InputGroup>
                    </InputGroupAddon>
                </InputGroup>
                </div>
            </StyledDiv>
        </React.Fragment>
    )
}

export default Landing;