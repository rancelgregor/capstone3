import React from 'react';
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,

  } from 'reactstrap';

import styled from 'styled-components';

const ImgNav = styled.img`
height: 100px;
width: 100px;
transition: transform 1.5s;
&:hover{
  transform: scale(1.2);
}
`;

const Links = styled.button`
margin-right: 2rem;
&:link,&:visited,&:hover,&:active{
  text-decoration: none;
  color: black;

}
`;


const NavApp = props => {
  
    return (
            <div>
              <Navbar style={{height: '10vh', backgroundColor: 'rgba(131,88,22,0.6166841736694677)'}} light expand="md">
                <NavbarBrand href="/">
                    <ImgNav src='/AppLogo.png' alt="" style = {{height: "80px"}}/>
                </NavbarBrand>
                  <Nav className="d-flex align-items-center h6" navbar>
                    <NavItem>
                      <Links as = "a" href="/">Rent A Book</Links>
                    </NavItem>
                    <NavItem>
                      <Links as="a" href="/">Libraries</Links>
                    </NavItem>
                  </Nav>
              </Navbar>
            </div>
);
}

export default NavApp;