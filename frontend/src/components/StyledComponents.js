
import styled from 'styled-components';

export const Sidebar = styled.div`
border: 2px solid black;
height: 100vh;
width: 310px;
left: -300px;
position: absolute;
transition: left 1s ease 0.1s;
border-top-right-radius: 10px;
border-bottom-right-radius: 10px;
background-color: rgba(166,106,22,0.5662640056022409);
z-index: 2;

&:hover{
left: 0;
}
`;

export const SidebarImg = styled.img`
height: 300px;
width: 300px;
border-radius: 20%;
border: 2px solid darkbrown
`;


export const ButtonLink = styled.button`
border-radius: 3px;
background-color: rgba(10,107,20,0.7539390756302521);
&:link,&:visited,&:hover,&:active{
    text-decoration: none;
    color: white;
`;
